import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var dropdown = document.getElementsByClassName("dropdown-btn");
var i1;

for (i1 = 0; i1 < dropdown.length; i1++) {
  dropdown[i1].addEventListener("click", function() 
  {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") 
  {
  dropdownContent.style.display = "none";
  }
   else 
  {
  dropdownContent.style.display = "block";
  }
  });
}
    // Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') 
  {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else 
  {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
  }

}
