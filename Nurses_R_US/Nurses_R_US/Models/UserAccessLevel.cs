﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class UserAccessLevel
    {
        public int AccessLevelId { get; set; }
        public int UserId { get; set; }

        public virtual AccessLevel AccessLevel { get; set; }
        public virtual User User { get; set; }
    }
}
