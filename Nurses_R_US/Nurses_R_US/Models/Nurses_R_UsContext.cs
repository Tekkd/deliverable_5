﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Nurses_R_US.Models
{
    public partial class Nurses_R_UsContext : DbContext
    {
        public Nurses_R_UsContext()
        {
        }

        public Nurses_R_UsContext(DbContextOptions<Nurses_R_UsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessLevel> AccessLevel { get; set; }
        public virtual DbSet<AccessLevelClaim> AccessLevelClaim { get; set; }
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountType> AccountType { get; set; }
        public virtual DbSet<ActiveUser> ActiveUser { get; set; }
        public virtual DbSet<Bank> Bank { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Complaint> Complaint { get; set; }
        public virtual DbSet<ComplaintStatus> ComplaintStatus { get; set; }
        public virtual DbSet<ComplaintType> ComplaintType { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeeStatus> EmployeeStatus { get; set; }
        public virtual DbSet<EmployeeType> EmployeeType { get; set; }
        public virtual DbSet<EmployeementHistory> EmployeementHistory { get; set; }
        public virtual DbSet<Geolocation> Geolocation { get; set; }
        public virtual DbSet<HdRate> HdRate { get; set; }
        public virtual DbSet<Hospital> Hospital { get; set; }
        public virtual DbSet<HospitalDepartment> HospitalDepartment { get; set; }
        public virtual DbSet<HospitalDepartmentStatus> HospitalDepartmentStatus { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<NextOfKin> NextOfKin { get; set; }
        public virtual DbSet<Nurse> Nurse { get; set; }
        public virtual DbSet<NurseAllocation> NurseAllocation { get; set; }
        public virtual DbSet<NurseDocument> NurseDocument { get; set; }
        public virtual DbSet<NurseRank> NurseRank { get; set; }
        public virtual DbSet<NurseRate> NurseRate { get; set; }
        public virtual DbSet<NurseSkill> NurseSkill { get; set; }
        public virtual DbSet<NurseStatus> NurseStatus { get; set; }
        public virtual DbSet<OrienationStatus> OrienationStatus { get; set; }
        public virtual DbSet<Orientation> Orientation { get; set; }
        public virtual DbSet<OrientationBooking> OrientationBooking { get; set; }
        public virtual DbSet<OrientationBookingStatus> OrientationBookingStatus { get; set; }
        public virtual DbSet<OrientationType> OrientationType { get; set; }
        public virtual DbSet<Password> Password { get; set; }
        public virtual DbSet<PostalCode> PostalCode { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<Qualification> Qualification { get; set; }
        public virtual DbSet<QualificationSkill> QualificationSkill { get; set; }
        public virtual DbSet<Rate> Rate { get; set; }
        public virtual DbSet<RateType> RateType { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<Request> Request { get; set; }
        public virtual DbSet<RequestStatus> RequestStatus { get; set; }
        public virtual DbSet<ShiftType> ShiftType { get; set; }
        public virtual DbSet<Skill> Skill { get; set; }
        public virtual DbSet<SkillType> SkillType { get; set; }
        public virtual DbSet<Suburb> Suburb { get; set; }
        public virtual DbSet<TertiaryInstitution> TertiaryInstitution { get; set; }
        public virtual DbSet<Title> Title { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserAccessLevel> UserAccessLevel { get; set; }
        public virtual DbSet<UserClaim> UserClaim { get; set; }
        public virtual DbSet<UserToken> UserToken { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=JARVISMB;Database=Nurses_R_Us;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AccessLevel>(entity =>
            {
                entity.ToTable("Access_Level");

                entity.Property(e => e.AccessLevelId).HasColumnName("Access_Level_ID");

                entity.Property(e => e.AccessLevelConcurrencyStamp)
                    .HasColumnName("Access_Level_Concurrency_Stamp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AccessLevelDescription)
                    .HasColumnName("Access_Level_Description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AccessLevelName)
                    .HasColumnName("Access_Level_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccessLevelNormalisedName)
                    .HasColumnName("Access_Level_NormalisedName")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AccessLevelClaim>(entity =>
            {
                entity.HasKey(e => e.AlcId)
                    .HasName("PK__Access_L__737E46CA65CA0940");

                entity.ToTable("Access_Level_Claim");

                entity.Property(e => e.AlcId).HasColumnName("ALC_ID");

                entity.Property(e => e.AccessLevelId).HasColumnName("Access_Level_ID");

                entity.Property(e => e.AlcClaimType)
                    .HasColumnName("ALC_Claim_Type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AlcValue)
                    .HasColumnName("ALC_Value")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccessLevel)
                    .WithMany(p => p.AccessLevelClaim)
                    .HasForeignKey(d => d.AccessLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Access_Le__Acces__3587F3E0");
            });

            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.AccountId).HasColumnName("Account_ID");

                entity.Property(e => e.AccountHolder)
                    .HasColumnName("Account_Holder")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("Account_Number")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.AccountTypeId).HasColumnName("Account_Type_ID");

                entity.Property(e => e.BankId).HasColumnName("Bank_ID");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.HasOne(d => d.AccountType)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.AccountTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Account__Account__5AB9788F");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Account__Bank_ID__59C55456");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.NurseId)
                    .HasConstraintName("FK__Account__Nurse_I__5BAD9CC8");
            });

            modelBuilder.Entity<AccountType>(entity =>
            {
                entity.ToTable("Account_Type");

                entity.Property(e => e.AccountTypeId).HasColumnName("Account_Type_ID");

                entity.Property(e => e.AccountTypeName)
                    .IsRequired()
                    .HasColumnName("Account_Type_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ActiveUser>(entity =>
            {
                entity.HasKey(e => e.ActUserId)
                    .HasName("PK__Active_U__BDB39FAD4AB6E0F3");

                entity.ToTable("Active_User");

                entity.Property(e => e.ActUserId).HasColumnName("Act_User_ID");

                entity.Property(e => e.SessionTime).HasColumnName("Session_Time");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.UserTokenId).HasColumnName("UserToken_ID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ActiveUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Active_Us__User___30C33EC3");
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.Property(e => e.BankId).HasColumnName("Bank_ID");

                entity.Property(e => e.BankCode).HasColumnName("Bank_Code");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasColumnName("Bank_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.Property(e => e.BranchId).HasColumnName("Branch_ID");

                entity.Property(e => e.BranchName)
                    .HasColumnName("Branch_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocationId).HasColumnName("Location_ID");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Branch)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Branch__Location__17036CC0");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.Property(e => e.CityId).HasColumnName("City_ID");

                entity.Property(e => e.CityName)
                    .HasColumnName("City_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvId).HasColumnName("Prov_ID");

                entity.HasOne(d => d.Prov)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.ProvId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__City__Prov_ID__01142BA1");
            });

            modelBuilder.Entity<Complaint>(entity =>
            {
                entity.HasKey(e => e.CmpltId)
                    .HasName("PK__Complain__396D08B46B64E1B4");

                entity.Property(e => e.CmpltId).HasColumnName("Cmplt_ID");

                entity.Property(e => e.CmpltDescription)
                    .IsRequired()
                    .HasColumnName("Cmplt_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltStatusId)
                    .HasColumnName("Cmplt_Status_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CmpltTypeId)
                    .HasColumnName("Cmplt_Type_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CmpltStatus)
                    .WithMany(p => p.Complaint)
                    .HasForeignKey(d => d.CmpltStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Complaint_Status");

                entity.HasOne(d => d.CmpltType)
                    .WithMany(p => p.Complaint)
                    .HasForeignKey(d => d.CmpltTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK");
            });

            modelBuilder.Entity<ComplaintStatus>(entity =>
            {
                entity.HasKey(e => e.CmpltStatusId)
                    .HasName("PK__Complain__51778012C4A71DFA");

                entity.ToTable("Complaint_Status");

                entity.Property(e => e.CmpltStatusId).HasColumnName("Cmplt_Status_ID");

                entity.Property(e => e.CmpltStatusDescription)
                    .HasColumnName("Cmplt_Status_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltStatusName)
                    .HasColumnName("Cmplt_Status_Name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ComplaintType>(entity =>
            {
                entity.HasKey(e => e.CmpltTypeId)
                    .HasName("PK__Complain__2E8E888DDFFE85AE");

                entity.ToTable("Complaint_Type");

                entity.Property(e => e.CmpltTypeId).HasColumnName("Cmplt_Type_ID");

                entity.Property(e => e.CmpltTypeDescription)
                    .HasColumnName("Cmplt_Type_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltTypeName)
                    .IsRequired()
                    .HasColumnName("Cmplt_Type_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.EmpId)
                    .HasName("PK__Employee__2623598B6793323A");

                entity.Property(e => e.EmpId).HasColumnName("Emp_ID");

                entity.Property(e => e.BranchId).HasColumnName("Branch_ID");

                entity.Property(e => e.EmpCellphone)
                    .IsRequired()
                    .HasColumnName("Emp_Cellphone")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.EmpDob)
                    .HasColumnName("Emp_DOB")
                    .HasColumnType("date");

                entity.Property(e => e.EmpEmail)
                    .IsRequired()
                    .HasColumnName("Emp_Email")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EmpName)
                    .IsRequired()
                    .HasColumnName("Emp_Name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EmpSId).HasColumnName("Emp_S_ID");

                entity.Property(e => e.EmpSurname)
                    .IsRequired()
                    .HasColumnName("Emp_Surname")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EmpTypeId).HasColumnName("Emp_Type_ID");

                entity.Property(e => e.LocationId).HasColumnName("Location_ID");

                entity.Property(e => e.TitleId).HasColumnName("Title_ID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__Branch__3C34F16F");

                entity.HasOne(d => d.EmpS)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.EmpSId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__Emp_S___3D2915A8");

                entity.HasOne(d => d.EmpType)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.EmpTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__Emp_Ty__395884C4");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__Locati__3B40CD36");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.TitleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__Title___3864608B");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Employee__User_I__3A4CA8FD");
            });

            modelBuilder.Entity<EmployeeStatus>(entity =>
            {
                entity.HasKey(e => e.EmpSId)
                    .HasName("PK__Employee__D18809EA034F1C3C");

                entity.ToTable("Employee_Status");

                entity.Property(e => e.EmpSId).HasColumnName("Emp_S_ID");

                entity.Property(e => e.EmpSDescription)
                    .HasColumnName("Emp_S_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.EmpSName)
                    .IsRequired()
                    .HasColumnName("Emp_S_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmployeeType>(entity =>
            {
                entity.HasKey(e => e.EmpTypeId)
                    .HasName("PK__Employee__61517AAA611040E7");

                entity.ToTable("Employee_Type");

                entity.Property(e => e.EmpTypeId).HasColumnName("Emp_Type_ID");

                entity.Property(e => e.EmpTypeDescription)
                    .HasColumnName("Emp_Type_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.EmpTypeName)
                    .IsRequired()
                    .HasColumnName("Emp_Type_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmployeementHistory>(entity =>
            {
                entity.HasKey(e => e.EhId)
                    .HasName("PK__Employee__DF760E3D1BCFA73C");

                entity.ToTable("Employeement_History");

                entity.Property(e => e.EhId)
                    .HasColumnName("EH_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateEnd)
                    .HasColumnName("Date_End")
                    .HasColumnType("date");

                entity.Property(e => e.DateStart)
                    .HasColumnName("Date_Start")
                    .HasColumnType("date");

                entity.Property(e => e.Employer)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.Position)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.EmployeementHistory)
                    .HasForeignKey(d => d.NurseId)
                    .HasConstraintName("FK__Employeem__Nurse__625A9A57");
            });

            modelBuilder.Entity<Geolocation>(entity =>
            {
                entity.HasKey(e => e.GeoLoId)
                    .HasName("PK__Geolocat__63DF321CDD72CC29");

                entity.Property(e => e.GeoLoId).HasColumnName("GeoLo_ID");

                entity.Property(e => e.CheckIn).HasColumnName("Check_In");

                entity.Property(e => e.CheckOut).HasColumnName("Check_Out");

                entity.Property(e => e.GeoloLatitude)
                    .HasColumnName("Geolo_Latitude")
                    .HasColumnType("decimal(8, 6)");

                entity.Property(e => e.GeoloLongitude)
                    .HasColumnName("Geolo_Longitude")
                    .HasColumnType("decimal(9, 6)");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.RequestId).HasColumnName("Request_ID");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.Geolocation)
                    .HasForeignKey(d => d.NurseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Geolocati__Nurse__70A8B9AE");

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.Geolocation)
                    .HasForeignKey(d => d.RequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Geolocati__Reque__719CDDE7");
            });

            modelBuilder.Entity<HdRate>(entity =>
            {
                entity.HasKey(e => new { e.HospDeptId, e.RateId })
                    .HasName("PK__HD_Rate__3794C085934BAEBD");

                entity.ToTable("HD_Rate");

                entity.Property(e => e.HospDeptId).HasColumnName("Hosp_Dept_ID");

                entity.Property(e => e.RateId).HasColumnName("Rate_ID");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.HospDept)
                    .WithMany(p => p.HdRate)
                    .HasForeignKey(d => d.HospDeptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HD_Rate__Hosp_De__4F7CD00D");

                entity.HasOne(d => d.Rate)
                    .WithMany(p => p.HdRate)
                    .HasForeignKey(d => d.RateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HD_Rate__Rate_ID__5070F446");
            });

            modelBuilder.Entity<Hospital>(entity =>
            {
                entity.HasKey(e => e.HospId)
                    .HasName("PK__Hospital__347D37F0E55AC0A4");

                entity.Property(e => e.HospId).HasColumnName("Hosp_ID");

                entity.Property(e => e.HospAddress)
                    .HasColumnName("Hosp_Address")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HospEmail)
                    .HasColumnName("Hosp_Email")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HospLatitude)
                    .HasColumnName("Hosp_Latitude")
                    .HasColumnType("decimal(8, 6)");

                entity.Property(e => e.HospLongitude)
                    .HasColumnName("Hosp_Longitude")
                    .HasColumnType("decimal(9, 6)");

                entity.Property(e => e.HospName)
                    .HasColumnName("Hosp_Name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HospTelephone)
                    .HasColumnName("Hosp_Telephone")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HospitalDepartment>(entity =>
            {
                entity.HasKey(e => e.HospDeptId)
                    .HasName("PK__Hospital__049F6D20EBDCE8C1");

                entity.ToTable("Hospital_Department");

                entity.Property(e => e.HospDeptId).HasColumnName("Hosp_Dept_ID");

                entity.Property(e => e.HdaCellphone)
                    .HasColumnName("HDA_Cellphone")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.HdaEmail)
                    .HasColumnName("HDA_Email")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HdaFname)
                    .HasColumnName("HDA_FName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HdaLname)
                    .HasColumnName("HDA_LName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HdaName)
                    .HasColumnName("HDA_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HdaTelephone)
                    .HasColumnName("HDA_Telephone")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.HdsId)
                    .HasColumnName("HDS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.HospId).HasColumnName("Hosp_ID");

                entity.Property(e => e.TitleId).HasColumnName("Title_ID");

                entity.HasOne(d => d.Hds)
                    .WithMany(p => p.HospitalDepartment)
                    .HasForeignKey(d => d.HdsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Hospital___HDS_I__46E78A0C");

                entity.HasOne(d => d.Hosp)
                    .WithMany(p => p.HospitalDepartment)
                    .HasForeignKey(d => d.HospId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Hospital___Hosp___48CFD27E");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.HospitalDepartment)
                    .HasForeignKey(d => d.TitleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Hospital___Title__47DBAE45");
            });

            modelBuilder.Entity<HospitalDepartmentStatus>(entity =>
            {
                entity.HasKey(e => e.HdsId)
                    .HasName("PK__Hospital__C747E923BB229AC1");

                entity.ToTable("Hospital_Department_Status");

                entity.Property(e => e.HdsId).HasColumnName("HDS_ID");

                entity.Property(e => e.HdsName)
                    .HasColumnName("HDS_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.LocationId).HasColumnName("Location_ID");

                entity.Property(e => e.Building)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CityId).HasColumnName("City_ID");

                entity.Property(e => e.PosCode).HasColumnName("Pos_Code");

                entity.Property(e => e.Street)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SuburbId).HasColumnName("Suburb_ID");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Location__City_I__07C12930");

                entity.HasOne(d => d.PosCodeNavigation)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.PosCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Location__Pos_Co__08B54D69");

                entity.HasOne(d => d.Suburb)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.SuburbId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Location__Suburb__09A971A2");
            });

            modelBuilder.Entity<NextOfKin>(entity =>
            {
                entity.HasKey(e => e.NokId)
                    .HasName("PK__Next_of___38E6A2B7DA7FC7DA");

                entity.ToTable("Next_of_Kin");

                entity.Property(e => e.NokId).HasColumnName("Nok_ID");

                entity.Property(e => e.Cellphone).HasColumnType("numeric(15, 0)");

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.Surname)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.NextOfKin)
                    .HasForeignKey(d => d.NurseId)
                    .HasConstraintName("FK__Next_of_K__Nurse__5224328E");
            });

            modelBuilder.Entity<Nurse>(entity =>
            {
                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.BranchId).HasColumnName("Branch_ID");

                entity.Property(e => e.LocationId).HasColumnName("Location_ID");

                entity.Property(e => e.NurseCellphone)
                    .HasColumnName("Nurse_Cellphone")
                    .HasColumnType("numeric(15, 0)");

                entity.Property(e => e.NurseDob)
                    .HasColumnName("Nurse_DOB")
                    .HasColumnType("date");

                entity.Property(e => e.NurseEmail)
                    .HasColumnName("Nurse_Email")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NurseFname)
                    .HasColumnName("Nurse_FName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NurseLname)
                    .HasColumnName("Nurse_LName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NurseStatusId).HasColumnName("Nurse_Status_ID");

                entity.Property(e => e.NurseTransport)
                    .HasColumnName("Nurse_Transport")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RankId).HasColumnName("Rank_ID");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("Tax_Number")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TitleId)
                    .HasColumnName("Title_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__Branch_ID__4B7734FF");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__Location___4A8310C6");

                entity.HasOne(d => d.NurseStatus)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.NurseStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__Nurse_Sta__4C6B5938");

                entity.HasOne(d => d.Rank)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.RankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__Rank_ID__489AC854");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.TitleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__Title_ID__47A6A41B");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Nurse)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse__User_ID__498EEC8D");
            });

            modelBuilder.Entity<NurseAllocation>(entity =>
            {
                entity.HasKey(e => new { e.NurseId, e.RequestId })
                    .HasName("PK__Nurse_Al__DA2EF94126B4E549");

                entity.ToTable("Nurse_Allocation");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.RequestId).HasColumnName("Request_ID");

                entity.Property(e => e.BookingConfirmation)
                    .HasColumnName("Booking_Confirmation")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CmpltId).HasColumnName("Cmplt_ID");

                entity.Property(e => e.RatingId).HasColumnName("Rating_ID");

                entity.HasOne(d => d.Cmplt)
                    .WithMany(p => p.NurseAllocation)
                    .HasForeignKey(d => d.CmpltId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse_All__Cmplt__6DCC4D03");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.NurseAllocation)
                    .HasForeignKey(d => d.NurseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse_All__Nurse__6AEFE058");

                entity.HasOne(d => d.Rating)
                    .WithMany(p => p.NurseAllocation)
                    .HasForeignKey(d => d.RatingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse_All__Ratin__6CD828CA");

                entity.HasOne(d => d.Request)
                    .WithMany(p => p.NurseAllocation)
                    .HasForeignKey(d => d.RequestId)
                    .HasConstraintName("FK__Nurse_All__Reque__6BE40491");
            });

            modelBuilder.Entity<NurseDocument>(entity =>
            {
                entity.HasKey(e => e.NdId)
                    .HasName("PK__Nurse_Do__930AE82A8FAC3879");

                entity.ToTable("Nurse_Document");

                entity.Property(e => e.NdId).HasColumnName("ND_ID");

                entity.Property(e => e.NdName)
                    .HasColumnName("ND_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NdPath)
                    .IsRequired()
                    .HasColumnName("ND_Path")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.NurseDocument)
                    .HasForeignKey(d => d.NurseId)
                    .HasConstraintName("FK__Nurse_Doc__Nurse__56E8E7AB");
            });

            modelBuilder.Entity<NurseRank>(entity =>
            {
                entity.HasKey(e => e.RankId)
                    .HasName("PK__Nurse_Ra__25BE3A6573F956E9");

                entity.ToTable("Nurse_Rank");

                entity.Property(e => e.RankId).HasColumnName("Rank_ID");

                entity.Property(e => e.RankDescription)
                    .HasColumnName("Rank_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RankName)
                    .HasColumnName("Rank_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NurseRate>(entity =>
            {
                entity.HasKey(e => e.RatesId)
                    .HasName("PK__Nurse_Ra__883D441628BA0F50");

                entity.ToTable("Nurse_Rate");

                entity.Property(e => e.RatesId).HasColumnName("Rates_ID");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.RatesAmount)
                    .HasColumnName("Rates_Amount")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.NurseRate)
                    .HasForeignKey(d => d.NurseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse_Rat__Nurse__5F7E2DAC");
            });

            modelBuilder.Entity<NurseSkill>(entity =>
            {
                entity.HasKey(e => new { e.NurseId, e.SkillId })
                    .HasName("PK__Nurse_Sk__EFF83C43FDA40BCB");

                entity.ToTable("Nurse_Skill");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.SkillId).HasColumnName("Skill_ID");

                entity.Property(e => e.DateObtained)
                    .HasColumnName("Date_Obtained")
                    .HasColumnType("date");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.NurseSkill)
                    .HasForeignKey(d => d.NurseId)
                    .HasConstraintName("FK__Nurse_Ski__Nurse__65370702");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.NurseSkill)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Nurse_Ski__Skill__662B2B3B");
            });

            modelBuilder.Entity<NurseStatus>(entity =>
            {
                entity.ToTable("Nurse_Status");

                entity.Property(e => e.NurseStatusId).HasColumnName("Nurse_Status_ID");

                entity.Property(e => e.NurseStatusName)
                    .IsRequired()
                    .HasColumnName("Nurse_Status_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrienationStatus>(entity =>
            {
                entity.HasKey(e => e.OsId)
                    .HasName("PK__Orienati__85A506EDEA839E99");

                entity.ToTable("Orienation_Status");

                entity.Property(e => e.OsId).HasColumnName("OS_ID");

                entity.Property(e => e.OsDescription)
                    .HasColumnName("OS_Description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OsName)
                    .HasColumnName("OS_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Orientation>(entity =>
            {
                entity.HasKey(e => e.OrientId)
                    .HasName("PK__Orientat__3E47E760FB17A254");

                entity.Property(e => e.OrientId).HasColumnName("Orient_ID");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrientName)
                    .HasColumnName("Orient_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrientTime).HasColumnName("Orient_Time");

                entity.Property(e => e.OsId)
                    .HasColumnName("OS_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.OtId)
                    .HasColumnName("OT_ID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Venue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Os)
                    .WithMany(p => p.Orientation)
                    .HasForeignKey(d => d.OsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OS_ID_Orient");

                entity.HasOne(d => d.Ot)
                    .WithMany(p => p.Orientation)
                    .HasForeignKey(d => d.OtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk_OT_ID_Orient");
            });

            modelBuilder.Entity<OrientationBooking>(entity =>
            {
                entity.HasKey(e => new { e.NurseId, e.OrientId })
                    .HasName("PK__Orientat__E756DC1E04B835E3");

                entity.ToTable("Orientation_Booking");

                entity.Property(e => e.NurseId).HasColumnName("Nurse_ID");

                entity.Property(e => e.OrientId).HasColumnName("Orient_ID");

                entity.Property(e => e.DateTime).HasColumnType("date");

                entity.Property(e => e.OrientBSId).HasColumnName("Orient_B_S_ID");

                entity.Property(e => e.TimeIn).HasColumnName("Time_In");

                entity.Property(e => e.TimeOut).HasColumnName("Time_Out");

                entity.HasOne(d => d.Nurse)
                    .WithMany(p => p.OrientationBooking)
                    .HasForeignKey(d => d.NurseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Orientati__Nurse__74794A92");

                entity.HasOne(d => d.OrientBS)
                    .WithMany(p => p.OrientationBooking)
                    .HasForeignKey(d => d.OrientBSId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Orientati__Orien__76619304");

                entity.HasOne(d => d.Orient)
                    .WithMany(p => p.OrientationBooking)
                    .HasForeignKey(d => d.OrientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Orientati__Orien__756D6ECB");
            });

            modelBuilder.Entity<OrientationBookingStatus>(entity =>
            {
                entity.HasKey(e => e.OrientBSId)
                    .HasName("PK__Orientat__7AA8079E16144F40");

                entity.ToTable("Orientation_Booking_Status");

                entity.Property(e => e.OrientBSId).HasColumnName("Orient_B_S_ID");

                entity.Property(e => e.OrientBSDescription)
                    .HasColumnName("Orient_B_S_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.OrientBSName)
                    .IsRequired()
                    .HasColumnName("Orient_B_S_Name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrientationType>(entity =>
            {
                entity.HasKey(e => e.OtId)
                    .HasName("PK__Orientat__A9E9ACD251AB3642");

                entity.ToTable("Orientation_Type");

                entity.Property(e => e.OtId).HasColumnName("OT_ID");

                entity.Property(e => e.OtDescription)
                    .HasColumnName("OT_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.OtName)
                    .IsRequired()
                    .HasColumnName("OT_Name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Password>(entity =>
            {
                entity.HasKey(e => new { e.PasswordId, e.LoginProvider })
                    .HasName("PK__Password__E83105F30E3CEC30");

                entity.Property(e => e.PasswordId)
                    .HasColumnName("Password_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Password1)
                    .HasColumnName("Password")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderDisplayName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Password)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Password__User_I__2B0A656D");
            });

            modelBuilder.Entity<PostalCode>(entity =>
            {
                entity.HasKey(e => e.PosCode)
                    .HasName("PK__Postal_C__84146D106D0221FD");

                entity.ToTable("Postal_Code");

                entity.Property(e => e.PosCode).HasColumnName("Pos_Code");

                entity.Property(e => e.PosCodeName)
                    .HasColumnName("Pos_Code_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.HasKey(e => e.ProvId)
                    .HasName("PK__Province__39116D9217DD0D98");

                entity.Property(e => e.ProvId).HasColumnName("Prov_ID");

                entity.Property(e => e.ProvName)
                    .HasColumnName("prov_Name")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Qualification>(entity =>
            {
                entity.HasKey(e => e.QualId)
                    .HasName("PK__Qualific__2D1FA19D2D75FC1D");

                entity.Property(e => e.QualId).HasColumnName("Qual_ID");

                entity.Property(e => e.QualDate)
                    .HasColumnName("Qual_Date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.QualDescription)
                    .HasColumnName("Qual_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.QualName)
                    .HasColumnName("Qual_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TiId).HasColumnName("TI_ID");

                entity.HasOne(d => d.Ti)
                    .WithMany(p => p.Qualification)
                    .HasForeignKey(d => d.TiId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Qualifica__TI_ID__10566F31");
            });

            modelBuilder.Entity<QualificationSkill>(entity =>
            {
                entity.HasKey(e => new { e.SkillId, e.QualId })
                    .HasName("PK__Qualific__767818A9E721532B");

                entity.ToTable("Qualification_Skill");

                entity.Property(e => e.SkillId).HasColumnName("Skill_ID");

                entity.Property(e => e.QualId).HasColumnName("Qual_ID");

                entity.HasOne(d => d.Qual)
                    .WithMany(p => p.QualificationSkill)
                    .HasForeignKey(d => d.QualId)
                    .HasConstraintName("FK__Qualifica__Qual___14270015");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.QualificationSkill)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Qualifica__Skill__1332DBDC");
            });

            modelBuilder.Entity<Rate>(entity =>
            {
                entity.HasKey(e => e.DepartmentRateId)
                    .HasName("PK__Rate__EB572759BAA651A1");

                entity.Property(e => e.DepartmentRateId).HasColumnName("Department_Rate_ID");

                entity.Property(e => e.DeptRateDescription)
                    .HasColumnName("Dept_Rate_Description")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.DeptRateName)
                    .HasColumnName("Dept_Rate_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RateAmount)
                    .HasColumnName("Rate_amount")
                    .HasColumnType("decimal(15, 0)");

                entity.Property(e => e.RateTypeId)
                    .HasColumnName("Rate_Type_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RateType)
                    .WithMany(p => p.Rate)
                    .HasForeignKey(d => d.RateTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rate__Rate_Type___3A81B327");
            });

            modelBuilder.Entity<RateType>(entity =>
            {
                entity.ToTable("Rate_Type");

                entity.Property(e => e.RateTypeId).HasColumnName("Rate_Type_ID");

                entity.Property(e => e.RateTypeDescription)
                    .HasColumnName("Rate_Type_Description")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.RateTypeName)
                    .HasColumnName("Rate_Type_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.Property(e => e.RatingId).HasColumnName("Rating_ID");

                entity.Property(e => e.RatingDescription)
                    .HasColumnName("Rating_Description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RatingLevel)
                    .HasColumnName("Rating_Level")
                    .HasColumnType("numeric(2, 0)");
            });

            modelBuilder.Entity<Request>(entity =>
            {
                entity.Property(e => e.RequestId).HasColumnName("Request_ID");

                entity.Property(e => e.DateFrom)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateTo).HasColumnType("date");

                entity.Property(e => e.HospDeptId).HasColumnName("Hosp_Dept_ID");

                entity.Property(e => e.NoNurse).HasColumnName("No_Nurse");

                entity.Property(e => e.RankId).HasColumnName("Rank_ID");

                entity.Property(e => e.ReqStatusId).HasColumnName("Req_Status_ID");

                entity.Property(e => e.ShiftTypeId).HasColumnName("Shift_Type_ID");

                entity.Property(e => e.SkillId).HasColumnName("Skill_ID");

                entity.HasOne(d => d.HospDept)
                    .WithMany(p => p.Request)
                    .HasForeignKey(d => d.HospDeptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Hosp_De__7A672E12");

                entity.HasOne(d => d.Rank)
                    .WithMany(p => p.Request)
                    .HasForeignKey(d => d.RankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Rank_ID__797309D9");

                entity.HasOne(d => d.ReqStatus)
                    .WithMany(p => p.Request)
                    .HasForeignKey(d => d.ReqStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Req_Sta__778AC167");

                entity.HasOne(d => d.ShiftType)
                    .WithMany(p => p.Request)
                    .HasForeignKey(d => d.ShiftTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Shift_T__76969D2E");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.Request)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Request__Skill_I__787EE5A0");
            });

            modelBuilder.Entity<RequestStatus>(entity =>
            {
                entity.HasKey(e => e.ReqStatusId)
                    .HasName("PK__Request___81EA1170A055A726");

                entity.ToTable("Request_Status");

                entity.Property(e => e.ReqStatusId).HasColumnName("Req_Status_ID");

                entity.Property(e => e.ReqStatusName)
                    .HasColumnName("Req_Status_Name")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ShiftType>(entity =>
            {
                entity.ToTable("Shift_Type");

                entity.Property(e => e.ShiftTypeId).HasColumnName("Shift_Type_ID");

                entity.Property(e => e.ShiftTypeDescription)
                    .HasColumnName("Shift_Type_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ShiftTypeName)
                    .HasColumnName("Shift_Type_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Skill>(entity =>
            {
                entity.Property(e => e.SkillId).HasColumnName("Skill_ID");

                entity.Property(e => e.SkillDescription)
                    .IsRequired()
                    .HasColumnName("Skill_Description")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SkillName)
                    .IsRequired()
                    .HasColumnName("Skill_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SkillTId)
                    .HasColumnName("Skill_T_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.SkillT)
                    .WithMany(p => p.Skill)
                    .HasForeignKey(d => d.SkillTId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Skill_Type");
            });

            modelBuilder.Entity<SkillType>(entity =>
            {
                entity.HasKey(e => e.SkillTId)
                    .HasName("PK__Skill_Ty__C80B3CB8BCFEFBA3");

                entity.ToTable("Skill_Type");

                entity.Property(e => e.SkillTId).HasColumnName("Skill_T_ID");

                entity.Property(e => e.SkillTDescription)
                    .HasColumnName("Skill_T_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SkillTName)
                    .HasColumnName("Skill_T_Name")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Suburb>(entity =>
            {
                entity.Property(e => e.SuburbId).HasColumnName("Suburb_ID");

                entity.Property(e => e.SuburbName)
                    .HasColumnName("Suburb_Name")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TertiaryInstitution>(entity =>
            {
                entity.HasKey(e => e.TiId)
                    .HasName("PK__Tertiary__4AFCCA5085FE0940");

                entity.ToTable("Tertiary_Institution");

                entity.Property(e => e.TiId).HasColumnName("TI_ID");

                entity.Property(e => e.LocationId).HasColumnName("Location_ID");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.TertiaryInstitution)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Tertiary___Locat__0C85DE4D");
            });

            modelBuilder.Entity<Title>(entity =>
            {
                entity.Property(e => e.TitleId).HasColumnName("Title_ID");

                entity.Property(e => e.TitleAbbreviation)
                    .HasColumnName("Title_Abbreviation")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.ConcurrencyStamp)
                    .IsRequired()
                    .HasColumnName("Concurrency_Stamp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnName("Date_Created")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Discriminator)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EmailConfirmed).HasColumnName("Email_Confirmed");

                entity.Property(e => e.FullName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LockoutEnd).HasColumnType("date");

                entity.Property(e => e.NormalisedEmail)
                    .HasColumnName("Normalised_Email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnName("Password_Hash")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SecurityStamp)
                    .IsRequired()
                    .HasColumnName("Security_Stamp")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasColumnName("User_Name")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UserNormalizedName)
                    .HasColumnName("User_Normalized_Name")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAccessLevel>(entity =>
            {
                entity.HasKey(e => new { e.AccessLevelId, e.UserId })
                    .HasName("PK__User_Acc__D75D62D12C86DD41");

                entity.ToTable("User_Access_Level");

                entity.Property(e => e.AccessLevelId).HasColumnName("Access_Level_ID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.AccessLevel)
                    .WithMany(p => p.UserAccessLevel)
                    .HasForeignKey(d => d.AccessLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__User_Acce__Acces__42E1EEFE");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAccessLevel)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__User_Acce__User___43D61337");
            });

            modelBuilder.Entity<UserClaim>(entity =>
            {
                entity.Property(e => e.UserClaimId).HasColumnName("UserClaim_ID");

                entity.Property(e => e.ClaimType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimValue)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("User_ID")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserClaim)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_ID_Claim");
            });

            modelBuilder.Entity<UserToken>(entity =>
            {
                entity.HasKey(e => new { e.UserTokenId, e.LoginProvider, e.Name })
                    .HasName("PK__UserToke__43C9FA778AC188DB");

                entity.Property(e => e.UserTokenId)
                    .HasColumnName("UserToken_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.LoginProvider)
                    .HasMaxLength(230)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(130)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.Value)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserToken)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserToken__User___2DE6D218");
            });
        }
    }
}
