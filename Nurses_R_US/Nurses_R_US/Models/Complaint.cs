﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Complaint
    {
        public Complaint()
        {
            NurseAllocation = new HashSet<NurseAllocation>();
        }

        public int CmpltId { get; set; }
        public string CmpltDescription { get; set; }
        public int CmpltStatusId { get; set; }
        public int CmpltTypeId { get; set; }

        public virtual ComplaintStatus CmpltStatus { get; set; }
        public virtual ComplaintType CmpltType { get; set; }
        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
    }
}
