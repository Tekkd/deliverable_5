﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseStatus
    {
        public NurseStatus()
        {
            Nurse = new HashSet<Nurse>();
        }

        public int NurseStatusId { get; set; }
        public string NurseStatusName { get; set; }

        public virtual ICollection<Nurse> Nurse { get; set; }
    }
}
