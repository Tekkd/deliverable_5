﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class HdRate
    {
        public int HospDeptId { get; set; }
        public int RateId { get; set; }
        public DateTime? Date { get; set; }

        public virtual HospitalDepartment HospDept { get; set; }
        public virtual Rate Rate { get; set; }
    }
}
