﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class QualificationSkill
    {
        public int SkillId { get; set; }
        public int QualId { get; set; }

        public virtual Qualification Qual { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
