﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class TertiaryInstitution
    {
        public TertiaryInstitution()
        {
            Qualification = new HashSet<Qualification>();
        }

        public int TiId { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ICollection<Qualification> Qualification { get; set; }
    }
}
