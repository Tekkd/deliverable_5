﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class OrientationBookingStatus
    {
        public OrientationBookingStatus()
        {
            OrientationBooking = new HashSet<OrientationBooking>();
        }

        public int OrientBSId { get; set; }
        public string OrientBSName { get; set; }
        public string OrientBSDescription { get; set; }

        public virtual ICollection<OrientationBooking> OrientationBooking { get; set; }
    }
}
