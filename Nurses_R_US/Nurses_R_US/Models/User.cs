﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class User
    {
        public User()
        {
            ActiveUser = new HashSet<ActiveUser>();
            Employee = new HashSet<Employee>();
            Nurse = new HashSet<Nurse>();
            Password = new HashSet<Password>();
            UserAccessLevel = new HashSet<UserAccessLevel>();
            UserClaim = new HashSet<UserClaim>();
            UserToken = new HashSet<UserToken>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserNormalizedName { get; set; }
        public DateTime DateCreated { get; set; }
        public string Email { get; set; }
        public string NormalisedEmail { get; set; }
        public int? EmailConfirmed { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public int? PhoneNumberConfirms { get; set; }
        public int? TwoFactorEnabled { get; set; }
        public DateTime? LockoutEnd { get; set; }
        public int? LockoutEnabled { get; set; }
        public int? AccessFailedCount { get; set; }
        public string Discriminator { get; set; }
        public string FullName { get; set; }

        public virtual ICollection<ActiveUser> ActiveUser { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<Password> Password { get; set; }
        public virtual ICollection<UserAccessLevel> UserAccessLevel { get; set; }
        public virtual ICollection<UserClaim> UserClaim { get; set; }
        public virtual ICollection<UserToken> UserToken { get; set; }
    }
}
