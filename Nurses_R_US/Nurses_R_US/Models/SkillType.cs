﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class SkillType
    {
        public SkillType()
        {
            Skill = new HashSet<Skill>();
        }

        public int SkillTId { get; set; }
        public string SkillTName { get; set; }
        public string SkillTDescription { get; set; }

        public virtual ICollection<Skill> Skill { get; set; }
    }
}
