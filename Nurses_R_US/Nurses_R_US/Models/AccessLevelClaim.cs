﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class AccessLevelClaim
    {
        public int AlcId { get; set; }
        public string AlcClaimType { get; set; }
        public string AlcValue { get; set; }
        public int AccessLevelId { get; set; }

        public virtual AccessLevel AccessLevel { get; set; }
    }
}
