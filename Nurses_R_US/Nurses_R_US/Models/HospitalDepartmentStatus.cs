﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class HospitalDepartmentStatus
    {
        public HospitalDepartmentStatus()
        {
            HospitalDepartment = new HashSet<HospitalDepartment>();
        }

        public int HdsId { get; set; }
        public string HdsName { get; set; }

        public virtual ICollection<HospitalDepartment> HospitalDepartment { get; set; }
    }
}
