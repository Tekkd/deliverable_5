﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseAllocation
    {
        public int NurseId { get; set; }
        public string BookingConfirmation { get; set; }
        public int CmpltId { get; set; }
        public int RequestId { get; set; }
        public int RatingId { get; set; }

        public virtual Complaint Cmplt { get; set; }
        public virtual Nurse Nurse { get; set; }
        public virtual Rating Rating { get; set; }
        public virtual Request Request { get; set; }
    }
}
