﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Province
    {
        public Province()
        {
            City = new HashSet<City>();
        }

        public int ProvId { get; set; }
        public string ProvName { get; set; }

        public virtual ICollection<City> City { get; set; }
    }
}
