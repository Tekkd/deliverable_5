﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Employee
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public string EmpSurname { get; set; }
        public string EmpCellphone { get; set; }
        public string EmpEmail { get; set; }
        public DateTime EmpDob { get; set; }
        public int TitleId { get; set; }
        public int EmpTypeId { get; set; }
        public int UserId { get; set; }
        public int LocationId { get; set; }
        public int BranchId { get; set; }
        public int EmpSId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual EmployeeStatus EmpS { get; set; }
        public virtual EmployeeType EmpType { get; set; }
        public virtual Location Location { get; set; }
        public virtual Title Title { get; set; }
        public virtual User User { get; set; }
    }
}
