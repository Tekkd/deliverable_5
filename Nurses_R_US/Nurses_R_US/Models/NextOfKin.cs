﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NextOfKin
    {
        public int NokId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public decimal? Cellphone { get; set; }
        public string Email { get; set; }
        public int NurseId { get; set; }

        public virtual Nurse Nurse { get; set; }
    }
}
