﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Bank
    {
        public Bank()
        {
            Account = new HashSet<Account>();
        }

        public int BankId { get; set; }
        public string BankName { get; set; }
        public int BankCode { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
