﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class ComplaintStatus
    {
        public ComplaintStatus()
        {
            Complaint = new HashSet<Complaint>();
        }

        public int CmpltStatusId { get; set; }
        public string CmpltStatusName { get; set; }
        public string CmpltStatusDescription { get; set; }

        public virtual ICollection<Complaint> Complaint { get; set; }
    }
}
