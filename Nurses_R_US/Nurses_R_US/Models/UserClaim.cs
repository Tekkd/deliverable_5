﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class UserClaim
    {
        public int UserClaimId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
