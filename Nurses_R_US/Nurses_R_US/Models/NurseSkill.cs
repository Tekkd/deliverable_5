﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseSkill
    {
        public int NurseId { get; set; }
        public int SkillId { get; set; }
        public DateTime? DateObtained { get; set; }

        public virtual Nurse Nurse { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
