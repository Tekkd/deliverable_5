﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Qualification
    {
        public Qualification()
        {
            QualificationSkill = new HashSet<QualificationSkill>();
        }

        public int QualId { get; set; }
        public string QualName { get; set; }
        public string QualDescription { get; set; }
        public DateTime QualDate { get; set; }
        public int TiId { get; set; }

        public virtual TertiaryInstitution Ti { get; set; }
        public virtual ICollection<QualificationSkill> QualificationSkill { get; set; }
    }
}
