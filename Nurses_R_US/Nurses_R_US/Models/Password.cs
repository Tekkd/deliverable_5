﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Password
    {
        public int PasswordId { get; set; }
        public string Password1 { get; set; }
        public int UserId { get; set; }
        public int LoginProvider { get; set; }
        public string ProviderDisplayName { get; set; }

        public virtual User User { get; set; }
    }
}
