﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Orientation
    {
        public Orientation()
        {
            OrientationBooking = new HashSet<OrientationBooking>();
        }

        public int OrientId { get; set; }
        public DateTime? Date { get; set; }
        public string OrientName { get; set; }
        public TimeSpan? OrientTime { get; set; }
        public string Venue { get; set; }
        public int OtId { get; set; }
        public int OsId { get; set; }

        public virtual OrienationStatus Os { get; set; }
        public virtual OrientationType Ot { get; set; }
        public virtual ICollection<OrientationBooking> OrientationBooking { get; set; }
    }
}
