﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class City
    {
        public City()
        {
            Location = new HashSet<Location>();
        }

        public int CityId { get; set; }
        public string CityName { get; set; }
        public int ProvId { get; set; }

        public virtual Province Prov { get; set; }
        public virtual ICollection<Location> Location { get; set; }
    }
}
