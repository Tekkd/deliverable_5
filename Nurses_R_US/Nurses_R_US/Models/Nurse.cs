﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Nurse
    {
        public Nurse()
        {
            Account = new HashSet<Account>();
            EmployeementHistory = new HashSet<EmployeementHistory>();
            Geolocation = new HashSet<Geolocation>();
            NextOfKin = new HashSet<NextOfKin>();
            NurseAllocation = new HashSet<NurseAllocation>();
            NurseDocument = new HashSet<NurseDocument>();
            NurseRate = new HashSet<NurseRate>();
            NurseSkill = new HashSet<NurseSkill>();
            OrientationBooking = new HashSet<OrientationBooking>();
        }

        public int NurseId { get; set; }
        public string NurseFname { get; set; }
        public string NurseLname { get; set; }
        public string NurseTransport { get; set; }
        public decimal? NurseCellphone { get; set; }
        public string NurseEmail { get; set; }
        public string TaxNumber { get; set; }
        public DateTime? NurseDob { get; set; }
        public int TitleId { get; set; }
        public int RankId { get; set; }
        public int UserId { get; set; }
        public int LocationId { get; set; }
        public int BranchId { get; set; }
        public int NurseStatusId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Location Location { get; set; }
        public virtual NurseStatus NurseStatus { get; set; }
        public virtual NurseRank Rank { get; set; }
        public virtual Title Title { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Account> Account { get; set; }
        public virtual ICollection<EmployeementHistory> EmployeementHistory { get; set; }
        public virtual ICollection<Geolocation> Geolocation { get; set; }
        public virtual ICollection<NextOfKin> NextOfKin { get; set; }
        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
        public virtual ICollection<NurseDocument> NurseDocument { get; set; }
        public virtual ICollection<NurseRate> NurseRate { get; set; }
        public virtual ICollection<NurseSkill> NurseSkill { get; set; }
        public virtual ICollection<OrientationBooking> OrientationBooking { get; set; }
    }
}
