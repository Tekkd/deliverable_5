﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class ActiveUser
    {
        public int ActUserId { get; set; }
        public int UserTokenId { get; set; }
        public TimeSpan? SessionTime { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
