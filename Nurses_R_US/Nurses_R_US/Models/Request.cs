﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Request
    {
        public Request()
        {
            Geolocation = new HashSet<Geolocation>();
            NurseAllocation = new HashSet<NurseAllocation>();
        }

        public int RequestId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int NoNurse { get; set; }
        public int ShiftTypeId { get; set; }
        public int ReqStatusId { get; set; }
        public int SkillId { get; set; }
        public int RankId { get; set; }
        public int HospDeptId { get; set; }

        public virtual HospitalDepartment HospDept { get; set; }
        public virtual NurseRank Rank { get; set; }
        public virtual RequestStatus ReqStatus { get; set; }
        public virtual ShiftType ShiftType { get; set; }
        public virtual Skill Skill { get; set; }
        public virtual ICollection<Geolocation> Geolocation { get; set; }
        public virtual ICollection<NurseAllocation> NurseAllocation { get; set; }
    }
}
