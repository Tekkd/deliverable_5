﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Branch
    {
        public Branch()
        {
            Employee = new HashSet<Employee>();
            Nurse = new HashSet<Nurse>();
        }

        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
    }
}
