﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class OrientationBooking
    {
        public int NurseId { get; set; }
        public int OrientId { get; set; }
        public int OrientBSId { get; set; }
        public DateTime? DateTime { get; set; }
        public TimeSpan? TimeIn { get; set; }
        public TimeSpan? TimeOut { get; set; }

        public virtual Nurse Nurse { get; set; }
        public virtual Orientation Orient { get; set; }
        public virtual OrientationBookingStatus OrientBS { get; set; }
    }
}
