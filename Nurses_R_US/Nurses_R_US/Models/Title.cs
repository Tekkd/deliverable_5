﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Title
    {
        public Title()
        {
            Employee = new HashSet<Employee>();
            HospitalDepartment = new HashSet<HospitalDepartment>();
            Nurse = new HashSet<Nurse>();
        }

        public int TitleId { get; set; }
        public string TitleAbbreviation { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<HospitalDepartment> HospitalDepartment { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
    }
}
