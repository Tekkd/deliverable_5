﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class ComplaintType
    {
        public ComplaintType()
        {
            Complaint = new HashSet<Complaint>();
        }

        public int CmpltTypeId { get; set; }
        public string CmpltTypeName { get; set; }
        public string CmpltTypeDescription { get; set; }

        public virtual ICollection<Complaint> Complaint { get; set; }
    }
}
