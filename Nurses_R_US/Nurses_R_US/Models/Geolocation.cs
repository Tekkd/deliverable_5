﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Geolocation
    {
        public int GeoLoId { get; set; }
        public decimal GeoloLongitude { get; set; }
        public decimal GeoloLatitude { get; set; }
        public TimeSpan CheckIn { get; set; }
        public TimeSpan CheckOut { get; set; }
        public int NurseId { get; set; }
        public int RequestId { get; set; }

        public virtual Nurse Nurse { get; set; }
        public virtual Request Request { get; set; }
    }
}
