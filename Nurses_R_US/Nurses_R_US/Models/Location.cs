﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Location
    {
        public Location()
        {
            Branch = new HashSet<Branch>();
            Employee = new HashSet<Employee>();
            Nurse = new HashSet<Nurse>();
            TertiaryInstitution = new HashSet<TertiaryInstitution>();
        }

        public int LocationId { get; set; }
        public string Building { get; set; }
        public string Street { get; set; }
        public int PosCode { get; set; }
        public int SuburbId { get; set; }
        public int CityId { get; set; }

        public virtual City City { get; set; }
        public virtual PostalCode PosCodeNavigation { get; set; }
        public virtual Suburb Suburb { get; set; }
        public virtual ICollection<Branch> Branch { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<TertiaryInstitution> TertiaryInstitution { get; set; }
    }
}
