﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseDocument
    {
        public int NdId { get; set; }
        public string NdName { get; set; }
        public string NdPath { get; set; }
        public int NurseId { get; set; }

        public virtual Nurse Nurse { get; set; }
    }
}
