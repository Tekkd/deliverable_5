﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseRank
    {
        public NurseRank()
        {
            Nurse = new HashSet<Nurse>();
            Request = new HashSet<Request>();
        }

        public int RankId { get; set; }
        public string RankName { get; set; }
        public string RankDescription { get; set; }

        public virtual ICollection<Nurse> Nurse { get; set; }
        public virtual ICollection<Request> Request { get; set; }
    }
}
