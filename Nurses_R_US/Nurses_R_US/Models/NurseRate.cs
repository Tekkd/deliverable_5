﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class NurseRate
    {
        public int RatesId { get; set; }
        public string RatesAmount { get; set; }
        public DateTime? Date { get; set; }
        public int NurseId { get; set; }

        public virtual Nurse Nurse { get; set; }
    }
}
