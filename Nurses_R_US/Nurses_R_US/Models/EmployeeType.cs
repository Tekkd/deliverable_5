﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class EmployeeType
    {
        public EmployeeType()
        {
            Employee = new HashSet<Employee>();
        }

        public int EmpTypeId { get; set; }
        public string EmpTypeName { get; set; }
        public string EmpTypeDescription { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
