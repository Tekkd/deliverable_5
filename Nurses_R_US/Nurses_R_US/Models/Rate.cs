﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Rate
    {
        public Rate()
        {
            HdRate = new HashSet<HdRate>();
        }

        public int DepartmentRateId { get; set; }
        public decimal? RateAmount { get; set; }
        public string DeptRateName { get; set; }
        public string DeptRateDescription { get; set; }
        public int RateTypeId { get; set; }

        public virtual RateType RateType { get; set; }
        public virtual ICollection<HdRate> HdRate { get; set; }
    }
}
