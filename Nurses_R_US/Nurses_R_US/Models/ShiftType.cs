﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class ShiftType
    {
        public ShiftType()
        {
            Request = new HashSet<Request>();
        }

        public int ShiftTypeId { get; set; }
        public string ShiftTypeName { get; set; }
        public string ShiftTypeDescription { get; set; }

        public virtual ICollection<Request> Request { get; set; }
    }
}
