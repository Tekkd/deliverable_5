﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Account
    {
        public int AccountId { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolder { get; set; }
        public int BankId { get; set; }
        public int AccountTypeId { get; set; }
        public int NurseId { get; set; }

        public virtual AccountType AccountType { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual Nurse Nurse { get; set; }
    }
}
