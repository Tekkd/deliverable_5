﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class UserToken
    {
        public int UserTokenId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
