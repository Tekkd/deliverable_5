﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class Suburb
    {
        public Suburb()
        {
            Location = new HashSet<Location>();
        }

        public int SuburbId { get; set; }
        public string SuburbName { get; set; }

        public virtual ICollection<Location> Location { get; set; }
    }
}
