﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class AccessLevel
    {
        public AccessLevel()
        {
            AccessLevelClaim = new HashSet<AccessLevelClaim>();
            UserAccessLevel = new HashSet<UserAccessLevel>();
        }

        public int AccessLevelId { get; set; }
        public string AccessLevelName { get; set; }
        public string AccessLevelDescription { get; set; }
        public string AccessLevelNormalisedName { get; set; }
        public string AccessLevelConcurrencyStamp { get; set; }

        public virtual ICollection<AccessLevelClaim> AccessLevelClaim { get; set; }
        public virtual ICollection<UserAccessLevel> UserAccessLevel { get; set; }
    }
}
