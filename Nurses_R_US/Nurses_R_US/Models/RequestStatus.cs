﻿using System;
using System.Collections.Generic;

namespace Nurses_R_US.Models
{
    public partial class RequestStatus
    {
        public RequestStatus()
        {
            Request = new HashSet<Request>();
        }

        public int ReqStatusId { get; set; }
        public string ReqStatusName { get; set; }

        public virtual ICollection<Request> Request { get; set; }
    }
}
