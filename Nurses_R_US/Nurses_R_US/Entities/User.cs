﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nurses_R_US.Entities
{
    public class User
    {
        public int User_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string User_Normalized_Name { get; set; }
        public DateTime Date_Created { get; set; }
        public string Email { get; set; }
        public string Normalised_Email { get; set; }
        public string Email_Confirmed { get; set; }
        public string Security_Stamp { get; set; }
        public string Concurrency_Stamp { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneNumberConfirms { get; set; }
        public int TwoFactorEnabled { get; set; }
        public DateTime LockoutEnd { get; set; }
        public int LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string Discriminator { get; set; }
        public string FullName { get; set; }
    }
}
