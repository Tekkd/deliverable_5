﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using Nurses_R_US.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Nurses_R_US.Services;
using Nurses_R_US.Dtos;
using Nurses_R_US.Models; 

namespace Nurses_R_US.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]

    //Security Stamp Attribute of users is Used to store the password for the User (Models)
    //Objects of the Model.User Class are named userDto 
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings; 

        public UsersController(
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value; 
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]UserDto userDto)
        {
            var user = _userService.Authenticate(userDto.Username, userDto.Password);

            if (user == null)
                return BadRequest(new { message = "Username or Password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescription);
            var tokenString = tokenHandler.WriteToken(token);

            //Return basic User info, without the password to store on Client Side
            return Ok(new
            {
                Id = user.UserId,
                Username = user.UserName,
                Fullname = user.FullName,
                Token = tokenString,
            }); 
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]UserDto userDto)
        {
            //map dto to User 
            var user = _mapper.Map<User>(userDto);

            try
            {
                //Save 
                _userService.Create(user, userDto.Password);
                return Ok(); 
            }
            catch (AppException ex)
            {
                //return error Message if there was an Exception
                return BadRequest(new { message = ex.Message }); 
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var userDtos = _mapper.Map<IList<UserDto>>(users);
            return Ok(userDtos); 
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);
            var userDto = _mapper.Map<UserDto>(user);
            return Ok(userDto); 
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UserDto userDto)
        {
            //map dto to Model.User and set id
            var user = _mapper.Map<User>(userDto);
            user.UserId = id;

            try
            {
                //Save
                _userService.Update(user, userDto.Password);
                return Ok(); 
            }
            catch (AppException ex)
            {
                //return error message if there was an exception 
                return BadRequest(new { message = ex.Message }); 
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok(); 
        }
    }
}