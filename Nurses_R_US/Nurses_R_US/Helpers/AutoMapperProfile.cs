﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Nurses_R_US.Dtos;
using Nurses_R_US.Models; 
//using Nurses_R_US.Entities; 

namespace Nurses_R_US.Helpers
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>(); 
        }
    }
}
