﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nurses_R_US.Helpers
{
    //Custom Exception Class for throwing application specific exceptions (e.g. for validation)
    //that can be caugt and handeled within the application
    public class AppException:Exception
    {
        public AppException() : base() { }
        public AppException(string message) : base(message) { }
        public AppException(string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args))
        {

        }
    }
}
