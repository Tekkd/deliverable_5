﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nurses_R_US.Dtos 
{
    public class UserDto
    {
        public int User_ID { get; set; } 
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
